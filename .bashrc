#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

alias notify-done=slack-cli

unset PROMPT_COMMAND
#export PROMPT_COMMAND='[\u@\h \W]\$ '
export TERM=xterm-256color
export PATH="${PATH}:/usr/local/bin:/home/william/.local/bin:/home/william/src/gdal/gdal/swig/java"

#export DISPLAY=:0.0

#export LD_LIBRARY_PATH="/usr/lib:/usr/local/lib:/usr/local/src/VTK/bin:/usr/local/lib64:/home/william/src/gdal/gdal/swig/java"
# /home/william/connectflow/bin:/home/william/connectflow/lib:/home/william/connectflow/mpi/bin:/home/william/connectflow/gcc/bin:

#export PATH="$PATH:/home/william/connectflow/bin:/home/william/connectflow/gcc/bin:/home/william/connectflow/mpi/bin"

#export SECURITYPATH_AMEC="/home/william/connectflow"

#export PYTHONPATH=$PYTHONPATH:/usr/local/src/VTK/Wrapping/Python/:/usr/local/src/VTK/Wrapping/Python/vtk:/usr/local/src/VTK/bin:/usr/local/lib/python2.7/site-packages/vtk:/home/william/py/flopyusg:/home/william/py/modflow-py:/home/william/py/geo

#:/home/william/src/ParaView-v5.0.0-source/build/lib:/home/william/src/ParaView-v5.0.0-source/build/lib/site-packages

# removed :/usr/lib/python2.7/site-packages:/usr/local/lib/python2.7/site-packages

#export CLASSPATH=/home/william/src/gdal/gdal/swig/java/gdal.jar:/home/william/src/gdal/gdal/swig/java/build/apps
export PATH="$PATH:/home/william/.local/bin"

export GISBASE="/opt/grass"
export PATH="$PATH:$GISBASE/bin:$GISBASE/scripts"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:/lib:/usr/lib:/usr/local/lib:/home/william/.local/lib"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$GISBASE/lib"
# for parallel session management, we use process ID (PID) as lock file number:
export GIS_LOCK=$$
# path to GRASS settings file
export GISRC="$HOME/.grass7/rc"

export PROJ_LIB="/usr/share/proj"
export PYTHONPATH="$PYTHONPATH:$GISBASE/etc/python:/home/william/py:"
export PYTHONPATH="$PYTHONPATH:/home/william/.local/lib/python3.6/site-packages"
export PYTHONPATH="$PYTHONPATH:/home/william/src/SWAcMod"

#export GRASS_PYTHON="/usr/bin/python2"
# changed 27/05/20
export GRASS_PYTHON="/usr/bin/python"

# export QT_QPA_PLATFORMTHEME="gtk2"
# export QT_STYLE_OVERRIDE=gtk2
export XDG_CURRENT_DESKTOP=GNOME
export QT_QPA_PLATFORMTHEME=qt5ct # in pam too
# export QT_STYLE_OVERRIDE=qt5ct
alias virtualbox='QT_STYLE_OVERRIDE=gtk2 virtualbox'

# for scroll wheel
export GDK_CORE_DEVICE_EVENTS=1

export EDITOR="/usr/bin/emacs"
export FCEDIT="$EDITOR"
export VISUAL="$EDITOR"
export SUDO_EDITOR="$EDITOR"
export SYSTEMD_EDITOR="emacs -nw"
export MAGICK_OCL_DEVICE="OFF" # emacs crashing a bit Oct 18

export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/local/lib64/pkgconfig"

export GDAL_CACHEMAX=512

export SBCL_HOME="${HOME}/.local/lib/sbcl"

if [ ${#INSIDE_EMACS} -gt 0 ]; then
    if [ -f "$HOME/.bash_ps1" ]; then
    	. "$HOME/.bash_ps1"
    fi
else
    function _update_ps1() { export PS1="$(/home/william/src/moontastic/moontastic.squish.lua $?)"; }
    export PROMPT_COMMAND="_update_ps1" #; $PROMPT_COMMAND"
    export MOONTHEME='arch'
fi

[ -n "$XTERM_VERSION" ] && transset-df -a  >/dev/null

#alias ls='ls -h --color'
#alias xbmc='kodi'
alias chromium='chromium --disk-cache-dir=/tmp/cache'

source /usr/share/bash-completion/bash_completion
complete -cf sudo

# powerline-daemon -q
# POWERLINE_BASH_CONTINUATION=1
# POWERLINE_BASH_SELECT=1
# . /usr/lib/python3.6/site-packages/powerline/bindings/bash/powerline.sh

# eval $(keychain --eval --quiet id_rsa)

eval "$(register-python-argcomplete slack-cli)"

alias lynx='lynx -cfg=~/.lynx.cfg -lss=~/.lynx.lss'

if [ -f /usr/bin/screenfetch ]; then screenfetch; fi
