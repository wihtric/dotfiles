;;; init.lisp --- nEXT config

;; Copyright © 2018 William Witterick <wihtric@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Sole init file

;;; Code:


(ql:quickload :cl-strings)

(in-package :next)

;; Use development platform port.
;; (setf *gtk-webkit-command*
;;       (format nil "/home/william/src/next/ports/gtk-webkit/next-gtk-webkit"
;;               (uiop:getenv "HOME")))

;; Start page
;; (setf *start-page-url* "https://duckduckgo.com/")
;; (setf *default-new-buffer-url* *start-page-url*)

;; (defun my-interface-defaults ()
;;   (setf (search-engines *interface*)
;;         '(("default" . "https://duckduckgo.com/?q=~a")
;;           ("yt" . "https://www.youtube.com/results?search_query=~a")
;;           ("w" . "https://en.wikipedia.org/w/index.php?search=~a")
;; 	  "s" . "https://www.startpage.com/do/dsearch?query=~a")))

;; Send to Emacs
(defun eval-in-emacs (&rest s-exps)
  "Evaluate S-EXPS with emacsclient."
  (let ((s-exps-string (cl-strings:replace-all
                        (write-to-string
                         `(progn ,@s-exps) :case :downcase)
                        ;; Discard the package prefix.
                        "next::" "")))
    (format *error-output* "Sending to Emacs:~%~a~%" s-exps-string)
    (uiop:run-program
     (list "emacsclient" "--eval" s-exps-string))))

;; Youtube download
;; (define-command youtube-dl-current-page ()
;;   "Download a video in the currently open buffer."
;;   (with-result (url (buffer-get-url))
;;     (eval-in-emacs
;;      (if (search "youtu" url)
;;          `(progn (youtube-dl ,url) (youtube-dl-list))
;;          `(youtube-dl-url ,url)))))

;; (defvar *global-map* (make-keymap)
;;   "My keymap.")

;; (define-key *global-map* (key "C-c d") 'youtube-dl-current-page)

;; ;; Emacsy next buffer
;; (define-key *global-map* (key "C-p") 'switch-buffer-previous)
;; (define-key *global-map* (key "C-n") 'switch-buffer-next)
;; (define-key *global-map* (key "C-x p") 'switch-buffer-previous)
;; (define-key *global-map* (key "C-x n") 'switch-buffer-next)

;; (define-key :keymap *global-map*
;;   "C-c d" #'youtube-dl-current-page
;;   "C-p" #'switch-buffer-previous
;;   "C-n" #'switch-buffer-next
;;   "C-x p" #'switch-buffer-previous
;;   "C-x n" #'switch-buffer-next)


;; Overload this so mini buffer goes away
;; (defmethod did-commit-navigation ((mode document-mode) url)
;;   (set-default-window-title)
;;   (add-or-traverse-history mode url)
;;   (echo *minibuffer* (concatenate 'string "Loading cunt: " url "."))
;;   (hide *minibuffer*))

(setf *minibuffer-style*
      (cl-css:css
       '((* :font-family "DejaVu Sans Mono for Powerline"
            :font-size "14pt"
	    :font-variant "Book")
         (body
	  :border-top "4px solid #4f97d7"
          :margin "0"
          :padding "0 6px"
	  :color "#292b2e")
         ("#container"
	  :display "flex"
          :flex-flow "column"
          :height "100%"
	  :color "#4f97d7")
         ("#input"
	  :padding "6px 0"
          :border-bottom "solid 1px lightgray"
	  :color "#292b2e")
         ("#completions"
	  :flex-grow "1"
	  :overflow-y "auto")
         ("#cursor"
	  :background-color "gray"
          :color "red")
         ("#prompt"
	  :padding-right "4px"
          :color "#4f97d7")
         (ul
	  :list-style "none"
          :padding "0"
          :margin "0")
         (li
	  :padding "2px")
         (.selected
	  :background-color "#4f97d7"
          :color "#292b2e"))))

;;; init.lisp ends here
