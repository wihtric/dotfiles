# set fish_function_path $fish_function_path "/usr/lib/python3.6/site-packages/powerline/bindings/fish"
# powerline-setup

set -g theme_display_git yes
set -g theme_display_git_untracked yes
set -g theme_display_git_ahead_verbose yes
#set -g theme_git_worktree_support yes
# set -g theme_display_vagrant yes
# set -g theme_display_docker_machine no
# set -g theme_display_hg yes
set -g theme_display_virtualenv yes
# set -g theme_display_ruby no
set -g theme_display_user yes
set -g theme_display_hostname yes
# set -g theme_display_vi no
set -g theme_display_date yes
set -g theme_display_cmd_duration yes
# set -g theme_title_display_process yes
# set -g theme_title_display_path no
# set -g theme_title_display_user yes
set -g theme_title_use_abbreviated_path yes
set -g theme_date_format "+%H:%M:%S"
# set -g theme_avoid_ambiguous_glyphs yes
#set -g theme_powerline_fonts yes
set -g theme_nerd_fonts yes
set -g theme_show_exit_status yes
# set -g default_user your_normal_user
set -g theme_color_scheme dark
# set -g fish_prompt_pwd_dir_length 0
# set -g theme_project_dir_length 1
set -g theme_newline_cursor no
set -g theme_display_vi no



#set -g theme_color_scheme user

      # #               light  medium  dark  darkest
      # #               ------ ------ ------ -------
      # set -l red      fb4934 cc241d
      # set -l green    b8bb26 98971a
      # set -l yellow   fabd2f d79921
      # set -l aqua     8ec07c 689d6a
      # set -l blue     0F94D2 458588
      # set -l grey     cccccc 999999 333333
      # set -l fg       fbf1c7 ebdbb2 d5c4a1 a89984
      # set -l bg       504945 282828
      # set white       FFFFFF

      # set -g __color_initial_segment_exit  $fg[1] $red[2] --bold
      # set -g __color_initial_segment_su    $fg[1] $green[2] --bold
      # set -g __color_initial_segment_jobs  $fg[1] $aqua[2] --bold

      # set -g __color_path                  $bg[1] $fg[2]
      # set -g __color_path_basename         $bg[1] $fg[2] --bold
      # set -g __color_path_nowrite          $red[1] $fg[2]
      # set -g __color_path_nowrite_basename $red[1] $fg[2] --bold

      # set -g __color_repo                  $green[2] $bg[1]
      # set -g __color_repo_work_tree        $bg[1] $fg[2] --bold
      # set -g __color_repo_dirty            $red[2] $fg[2]
      # set -g __color_repo_staged           $yellow[1] $bg[1]

      # set -g __color_vi_mode_default       $fg[4] $bg[2] --bold
      # set -g __color_vi_mode_insert        $blue[1] $bg[2] --bold
      # set -g __color_vi_mode_visual        $yellow[1] $bg[2] --bold

      # set -g __color_vagrant               $blue[2] $fg[2] --bold
      # set -g __color_username              $blue[1] $white[1]
      # set -g __color_rvm                   $red[2] $fg[2] --bold
      # set -g __color_virtualfish           $blue[2] $fg[2] --bold
# set MPD_HOST npi
# set MPD_PORT 6600
alias music='tmux new-session "tmux source-file ~/.ncmpcpp/tmux_session"' # Tmux session with ncmpcpp and cover art
# screenfetch
neofetch

source /usr/share/icons-in-terminal/icons.fish

alias ndb="kitty +kitten ssh william@ndb"
