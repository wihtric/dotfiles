;;; groups.lisp --- Stumpwm groups config

;; Copyright © 2018 William Witterick <wihtric@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Groups config

;;; Code:

(in-package :stumpwm)

;; switching groups
(define-key stumpwm:*top-map* (stumpwm:kbd "M-Left") "gprev")
(define-key stumpwm:*top-map* (stumpwm:kbd "M-Right") "gnext")
(define-key stumpwm:*top-map* (stumpwm:kbd "M-F1") "gselect 1")
(define-key stumpwm:*top-map* (stumpwm:kbd "M-F2") "gselect 2")
(define-key stumpwm:*top-map* (stumpwm:kbd "M-F3") "gselect 3")
(define-key stumpwm:*top-map* (stumpwm:kbd "M-F4") "gselect 4")
(define-key stumpwm:*top-map* (stumpwm:kbd "M-F5") "gselect 5")
(define-key stumpwm:*top-map* (stumpwm:kbd "M-F6") "gselect 6")

(defmacro select-group-or-create ((name &key (float nil)) &body body)
  (let ((name-sym (gensym "NAME-"))
        (group-sym (gensym "GROUP-")))
    `(let* ((,name-sym ,name)
            (,group-sym (find ,name-sym
			      (screen-groups
			       (current-screen))
			      :key #'group-name :test #'equal)))
       (if ,group-sym
           (gselect ,group-sym)
           (progn
             ,(if float
                  `(gnew-float ,name-sym)
                  `(gnew ,name-sym))
             (progn
               ,@body))))))

(run-commands "grename code")
(gnewbg-float "chat")
(gnewbg-float "gis")
(gnewbg "win")
(gnewbg "www")
(gnewbg "term")


;; Clear rules
(clear-window-placement-rules)

(define-frame-preference "code"
  ;; frame raise lock (lock AND raise == jumpto)
  (0 t   t :title "emacs"))

(define-frame-preference "www"
  ;; frame raise lock (lock AND raise == jumpto)
;;    (0 t nil :class "chromium" :instance "chromium")
  (0 t nil :class "brave-browser" :role "browser"
     ;;(1 t   t :class "Firefox")
     ))

  (define-frame-preference "chat"
    ;; frame raise lock (lock AND raise == jumpto)
    (0 nil nil :class "slack")
    (1 nil nil :class "pavucontrol")
    (2 nil nil :class "skype"))

;; (define-frame-preference "term"
;;   ;; frame raise lock (lock AND raise == jumpto)
;;   (1 t t :class "kitty" :instance "kitty" :title "htop")
;;   (0 t t :title "mf6" :class "kitty" :instance "kitty")
;;   ;;(1 t t :title "mf6")
;;   (2 t t :class "Termite" :title "tail -f mfsim.lst"))

(define-frame-preference "term"
  (2 t t :class "mf6")
  (1 t t :class "htop")
  (0 t t :class "watch"))


(refresh-heads)

(defun other-hidden-window (group)
  "Return the last window that was accessed and that is hidden."
  (let ((wins (remove-if
               (lambda (w)
                 (or (eq (frame-window (window-frame w)) w)
                     (eq (nth 0 (sort-windows group)) w)))
               (group-windows group))))
    (first wins)))

;;; groups.lisp ends here
