;;; windows.lisp --- Stumpwm windows config

;; Copyright © 2018 William Witterick <wihtric@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(in-package :stumpwm)


(defun other-hidden-window (group)
  "Return the last window that was accessed and that is hidden."
  (let ((wins (remove-if
               (lambda (w)
                 (or (eq (frame-window (window-frame w)) w)
                     (eq (nth 0 (sort-windows group)) w)))
               (group-windows group))))
    (first wins)))


;;;; Windows
(set-normal-gravity :center)
(set-float-focus-color "#0F94D2")
(set-focus-color "#0F94D2")
(set-unfocus-color "black")
(set-float-unfocus-color "black")

;; Don't show startup message.
;; (setf *startup-message* "3===D-")
(setf *startup-message* nil)
(setf *data-dir* "~/.stumpwm.d/data/")

(setf *message-window-gravity* :center)
(setf *input-window-gravity* :center)

(setf *normal-border-width* 0)
(setf *maxsize-border-width* 0)
(setf *timeout-wait* 10)

(load-module "ttf-fonts")

(set-font
 (make-instance 'xft:font
		:family "DejaVu Sans Mono for Powerline"
		:subfamily "Book"
		:size 13))

;;Colors for the input box; these should fairly self-explanatory.
(set-bg-color "#292b2e")
(set-fg-color "#0F94D2")
;; #eead0e -- Merckx orange
;; "#5D4D7A" -- spacemacs purple
(set-border-color "#5D4D7A")

;;I just don't like zero indexing frames/windows. 0 is not next to 1
;;on the keyboard!  See
;;<http://lists.gnu.org/archive/html/stumpwm-devel/2006-08/msg00002.html>
(setf *frame-number-map* "1234567890") ;doesn't seem to work right now

;;; windows.lisp ends here
