;;; startup.lisp --- Stumpwm initial applications

;; Copyright © 2018 William Witterick <wihtric@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Applications at startup

;;; Code:

(in-package :stumpwm)

(ql:quickload :xml-emitter)
;; (ql:quickload :dbus)
(ql:quickload :bordeaux-threads)

(load-module "notify")

(setf notify:*notify-server-title-color* "^9")
(setf notify:*notify-server-body-color* "^n")
(notify:notify-server-on)

(defun play-alert (app icon summary body)
  (declare (ignore app icon summary body))
  (run-shell-command "aplay /usr/lib/sky/sounds/new_im.wav" t))

(stumpwm:add-hook notify::*notification-received-hook* 'play-alert)

(setf notify::*notify-server-start-message* "notificaciones habilitadas")

(setf notify::*notify-server-max-line-length* 80)

(define-key *root-map* (kbd "SPC") "notify-server-toggle")

(setf *openweather-loc-defaults* '("Shrewsbury,GB" "London,GB"))
;;(select-group-or-create ("code" :float nil))

(in-package command-history)

(setf command-history::*command-history-file*
      (merge-pathnames *data-dir* "history"))


(run-shell-command "pasystray")
;; (weather)
;;(emacs-run)
;;(bgname)
;;(pavucontrol)

;;; startup.lisp ends here
