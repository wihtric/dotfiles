;;; mode-line.lisp --- Stumpwm mode-line config

;; Copyright © 2018 William Witterick <wihtric@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Standard mode-line with icons

;;; Code:

;;(sb-thread:with-mutex (*lock*)
(load "~/quicklisp/setup.lisp")
(ql:quickload :clx-truetype)
(ql:quickload :xembed) ;; Required by stumptray


  (in-package :stumpwm)

  ;; (in-package :mem)
  ;; (add-screen-mode-line-formatter #\m 'fmt-mem-usage)
  ;; (add-screen-mode-line-formatter #\m 'mem-modeline)
  ;; (setf *mem-modeline-fmt* "%m")

  (in-package :maildir)
  (stumpwm:add-screen-mode-line-formatter #\m 'maildir-modeline)
  ;;(setf *maildir-path* #P"/home/william/.mail/work/")
  (setf *maildir-alist* (list (cons "Mail" #P"/home/william/.mail/work/")))
  (setf *maildir-update-time* 20)
  ;; ;; fix pull request #111
  ;; (setf *maildir-formatters-alist*
  ;;   '((#\n maildir-get-new)
  ;;     (#\c maildir-get-cur)
  ;;     (#\t maildir-get-tmp)))
  (setf *maildir-modeline-fmt* "%l: %n")
  ;; (setf *maildir-modeline-fmt* "%M")

(in-package cpu)
;; widescreen
;;(setf *cpu-modeline-fmt* "%t %c")
;; vertical
(in-package cpu)
(defun fmt-cpu-usage ()
  "Returns a string representing current the percent of average CPU
  utilization."
  (let ((cpu (truncate (* 100 (current-cpu-usage)))))
    (format nil "CPU:^[~A~3D%^]" (bar-zone-color cpu) cpu)))

(defun fmt-cpu-temp ()
  "Returns a string representing the current CPU temperature."
  (format nil "~d°C"
          (case (car *acpi-thermal-zone*)
            (:procfs (parse-integer
                      (get-proc-file-field (cdr *acpi-thermal-zone*) "temperature")
                      :junk-allowed t))
            (:sysfs   (with-open-file (f (cdr *acpi-thermal-zone*))
                        (/ (read f) 1000))))))

(setf *cpu-modeline-fmt* "%c %t ")



  (in-package disk)
  ;; (setf *disk-modeline-fmt* "%D")
  ;; (setf *disk-modeline-fmt* "%u")
  (setf *disk-modeline-fmt* "%m: %p")
  (add-screen-mode-line-formatter #\D 'disk-modeline)

  (in-package :stumpwm)
  (setf *mode-line-timeout* 10)

  ;;"Define mode line foreground color."
  ;;(setf *mode-line-foreground-color* "#0F94D2")

  ;;"Define mode line border color."
  (setf *mode-line-border-color* "#0F94D2")

  (setf *mode-line-foreground-color* "#4f97d7")
  ;; (setf *mode-line-background-color* "#5D4D7A") spacemacs purple
  (setf *mode-line-background-color* "#292b2e") ; grey bg

  (setf *bar-med-color* "^B^2*")
  (setf *bar-hi-color* "^B^9*")
  (setf *bar-crit-color* "^B^1*")

  ;;"How thick shall the mode line border be?"
  (setf *mode-line-border-width* 2)

  ;;"How much padding should be between the mode line text and the sides?"
  (setf *mode-line-pad-x* 3)
  (setf *mode-line-pad-y* 0)

  ;; Turn on the modeline
  (if (not (head-mode-line (current-head)))
      (toggle-mode-line (current-screen) (current-head)))

  (setf *group-format* "%t")
  (setf *window-format* "%n%s%20t")
;;;;;;;;;(setf *mode-line-timeout* 2)

  ;;(stumptray:stumptray)

  ;; Show some stuff in the modeline
  ;; (setf *screen-mode-line-format*
  ;;       (list '(:eval (run-shell-command "date '+%R %F %a'|tr -d [:cntrl:]" t)) " | %I | %t | %c| %B | %D| %m| ^B%M^b | %g | %W %T"))

  ;; (setf *screen-mode-line-format*
  ;;       (list '(:eval (run-shell-command "date '+%R %F %a'|tr -d [:cntrl:]" t)) " | %I | %t | %c| %B | %D| %m| ^B%M^b | %g | %W %T"))

;; widescreen
  ;; (setf *screen-mode-line-format*
  ;; 	(list '(:eval (run-shell-command "date '+%R %F %a'|tr -d [:cntrl:]" t)) " | %I | %C| %B | %D| %M| ^B%m^b | %g | %W %T"))


;; vertical
(in-package :mem)
(setf *mem-modeline-fmt* "MEM:%p")
  (setf *screen-mode-line-format*
	(list '(:eval (run-shell-command "date '+%R %F %a'|tr -d [:cntrl:]" t))
	      " | %C| %B | %D| %M| ^B%m^b |"))

  (load-module "stumptray")
  ;;(in-package :stumptray)
  ;;(setf *tray-win-background* "#5D4D7A")
  ;;(setf *tray-viwin-background* "#5D4D7A")

  (defcommand ww-toggle-mode-line () ()
    "Toggle the mode line in StumpWM."
    (setf *mode-line-background-color* stumpwm:*mode-line-background-color*)
    (setf stumptray:*tray-viwin-background* stumpwm:*mode-line-background-color*)
    (toggle-mode-line (current-screen) (current-head))
    (setf *mode-line-background-color* stumpwm:*mode-line-background-color*)
    (setf stumptray:*tray-viwin-background* stumpwm:*mode-line-background-color*)
    )
  (define-key *root-map* (kbd "q") "ww-toggle-mode-line")


;;; mode-line.lisp ends here
