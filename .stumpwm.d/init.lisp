;;; init.lisp --- Stumpwm config

;; Copyright © 2018 William Witterick <wihtric@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Main init, loads other files

;;; Code:


(in-package :stumpwm)

;; load a few modules
(mapcar #'load-module '("cpu"
                        "battery-portable"
                        "disk"
			"maildir"
			"wifi"
			"mem"
			"maildir"
			"end-session"
			"command-history"))

(load "~/quicklisp/setup.lisp")

;; (setf *colors*
;;       '("black"
;; 	"red"
;; 	"green"
;; 	"yellow"
;; 	"blue"
;; 	"magenta"
;; 	"cyan"
;; 	"white"))

(setf *colors* (append *colors*
                       (list "GreenYellow"
                             "#0F94D2")))

(update-color-map (current-screen))

(setf *grab-pointer-foreground* (xlib:make-color :red 0.1 :green 0.25 :blue 0.5))
(setf *grab-pointer-character* 88)
(setf *grab-pointer-character-mask* 88)

;;(run-shell-command "feh --bg-scale '/home/william/.stumpwm.d/pic/spurs_wallpaper.jpg'")
(run-shell-command "feh --bg-scale '/home/william/.stumpwm.d/pic/sharks.jpg'")

;; Load swank.
(load "/usr/share/emacs/site-lisp/slime/swank-loader.lisp")
(swank-loader:init)
(defcommand swank () ()
    (swank:create-server :port 4005
                       :style swank:*communication-style*
                       :dont-close t))
(swank)

(load "~/.stumpwm.d/windows.lisp")
(load "~/.stumpwm.d/groups.lisp")
(load "~/.stumpwm.d/keybindings.lisp")
(load "~/.stumpwm.d/mode-line.lisp")
(load "~/.stumpwm.d/weather.lisp")
(load "~/.stumpwm.d/mf6.lisp")
;;(load "~/.stumpwm.d/command-history.lisp")
(load "~/.stumpwm.d/startup.lisp")

(icon-tray)

;;; init.lisp ends here
