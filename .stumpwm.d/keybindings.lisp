;;; keybindings.lisp --- Stumpwm keys config

;; Copyright © 2018 William Witterick <wihtric@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Keybindings &c.

;;; Code:

(in-package :stumpwm)

;; escape key
(stumpwm:set-prefix-key (stumpwm:kbd "C-;"))

;; disk usage
(defcommand baobab () ()
  "Start baobab."
  (run-or-raise "baobab" '(:class "baobab")))
(define-key *root-map* (kbd "u") "baobab")

(defcommand paraview () ()
  "Start Paraview."
  (run-or-raise "paraview" '(:class "paraview")))
(define-key *root-map* (kbd "p") "paraview")

;; termite
(define-key *root-map* (kbd "c") "exec kitty")

;; kitty ssh
;; (defcommand kitty-ssh () ()
;;   (run-or-raise "kitty +kitten ssh william@ndb" '(:class "kitty"))
;;   )
;; (define-key *root-map* (kbd "C") "kitty-ssh")

;; Screen dump
(defcommand dumpme () ()
  "Dump the screen."
  (run-or-raise "import -window root ~/pic/screendump.png" '(:class "DumpMe"))
  (sleep 1)
  (run-shell-command
   "xclip -selection clipboard -t image/png -i '/home/william/pic/screendump.png'"))

(define-key *root-map* (kbd "d") "dumpme")


;; battery
(defcommand show-battery () ()
  (echo-string (current-screen) (run-shell-command "acpi" t)))
(define-key *root-map* (kbd "b") "show-battery")

;; banish
(define-key stumpwm:*root-map* (stumpwm:kbd "B") "banish")

;; remove cos of elpy clash
(undefine-key stumpwm:*root-map* (stumpwm:kbd "M-Left"))
(undefine-key stumpwm:*root-map* (stumpwm:kbd "M-Right"))
(undefine-key stumpwm:*top-map* (stumpwm:kbd "M-Left"))
(undefine-key stumpwm:*top-map* (stumpwm:kbd "M-Right"))



(define-key stumpwm:*root-map* (stumpwm:kbd "Left") "prev")
(define-key stumpwm:*root-map* (stumpwm:kbd "Right") "next")
(define-key *top-map* (stumpwm:kbd "s-TAB") "next")
(define-key *top-map* (stumpwm:kbd "s-S-TAB") "next")
(define-key stumpwm:*root-map* (stumpwm:kbd "C-Left") "prev")
(define-key stumpwm:*root-map* (stumpwm:kbd "C-Right") "next")

;;(move-focus)

(run-shell-command "setxkbmap -model \"pc105\" -layout \"gb,us\" -option \"grp:caps_toggle,grp_led:scroll\"")

;; this won't change over UK=>US
(define-key stumpwm:*root-map* (stumpwm:kbd "@") "windowlist")

;; launch qgis ----------------------------------------------------------------
(defcommand qgis () ()
	    "Start QGIS or switch to it, if it is already running."
	    (select-group-or-create ("gis" :float t))
	    (run-or-raise "qgis-git" '(:class "qgis")))
(define-key *root-map* (kbd "g") "qgis")

;; launch Web browser
(defcommand brave () ()
	    "Start Chromium or switch to it, if it is already running."
	    ;;	    (select-group-or-create ("www" :float nil))
	    (gselect "www")
	    (run-or-raise "brave" '(:class "brave-browser")))
;; (define-key *root-map* (kbd "i") "brave")

(defcommand slack-run () ()
	    "Start slack or switch to it, if it is already running."
	    (select-group-or-create ("chat" :float t))
	    (run-or-raise "slack" '(:class "slack"))

	    (defcommand skype-run () ()
			"Start skype or switch to it, if it is already running."
			(select-group-or-create ("chat" :float t))
			(run-or-raise "skypeforlinux" '(:class "skype"))))

(defcommand pavucontrol () ()
	    "Start y or switch to it, if it is already running."
	    (select-group-or-create ("chat" :float t))
  (run-or-raise "pavucontrol" '(:class "pavucontrol")))

(defcommand bgname () ()
	    (select-window-by-number 0)
	    (title "")
	    (pull-hidden-other))

(defcommand emacs-run () ()
  "Start emacs or switch to it, if it is already running."
  ;;(select-group-or-create ("code" :float nil))
  ;; (run-shell-command "mpv --ao=null -loop --no-keepaspect --geometry=1920x1024 ~/.stumpwm.d/pic/rain3.mp4")
  ;; (title "")
  ;; (sleep 0.2)
  (run-or-raise "emacsclient -c" '(:class "Emacs"))
  ;; (sleep 0.2)
  ;;(pull-window (nth 0 (sort-windows (current-group))))
  ;; (select-window-by-number 0)
  ;; ;; (title "")
  ;; (pull-hidden-other)
  )
(define-key *root-map* (kbd "e") "emacs-run")

(define-key *root-map* (kbd "w") "weather")

;; dropbox --------------------------------------------------------------------
(defcommand db-status () ()
	    (run-shell-command "dropbox-cli status" t))
(define-key *root-map* (kbd "C-s") "db-status")

(defcommand db-start () ()
 (run-shell-command "dropbox-cli start" t))
(define-key *root-map* (kbd "C-a") "db-start")

(defcommand db-stop () ()
 (run-shell-command "dropbox-cli stop" t))
(define-key *root-map* (kbd "C-d") "db-stop")

;; monitors -------------------------------------------------------------------
(defcommand secondary-monitor () ()
 (run-shell-command "mons -s"))
(define-key *root-map* (kbd "M-m") "secondary-monitor")

(defcommand primary-monitor () ()
 (run-shell-command "mons -o"))
(define-key *root-map* (kbd "C-m") "primary-monitor")

;; icons --------------------------------------------------------------------
(defcommand icon-tray () ()
  (run-commands "stumptray")
  )
(define-key *root-map* (kbd "C-i") "icon-tray")

(defcommand dump-select () ()
  ;; (run-shell-command "scrot -s ~/pic/screen-selection.jpg")
  (run-shell-command "gscreenshot"))
(define-key *root-map* (kbd "M-d") "dump-select")

(define-key stumpwm:*top-map* (stumpwm:kbd "M-q") "end-session")
;;; keybindings.lisp ends here
