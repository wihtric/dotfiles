;;; mf6.lisp --- 
;; 
;; Filename: mf6.lisp
;; Description: 
;; Author: William Witterick
;; Maintainer: 
;; Created: Mon Oct 21 10:35:32 2019 (+0100)
;; Version: 
;; Last-Updated: 
;;           By: 
;;     Update #: 0
;; URL: 
;; Keywords: 
;; Compatibility: 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Commentary: 
;; 
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Change Log:
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Code:

(in-package :stumpwm)

(defvar *mf6-running-p* nil)
(defvar *mf6-timer* nil)


(defun start-mf6-timer (path)
  "doc"
  (setf *mf6-timer* (run-with-timer 20 30 #'mf6-slack path)))


(define-condition kbd-parse ()
  ((text :initarg :text :reader text)))

(defun kbd? (string)
  "takes a string and returns the result of calling {kbd}
with string, or if it isnt valid return nil."
  (handler-case (kbd string)
	(kbd-parse-error () nil)
	(kbd-parse () nil)))

(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
       while line
       collect (list line))))


(defun print-thread-info ()
  (let* ((curr-thread sb-thread:*current-thread*)
         (curr-thread-name (sb-thread:thread-name curr-thread))
         (all-threads (sb-thread:list-all-threads)))
    (format t "Current thread: ~a~%~%" curr-thread)
    (format t "Current thread name: ~a~%~%" curr-thread-name)
    (format t "All threads:~% ~{~a~%~}~%" all-threads))
  nil)

(defun mf6-run (path)
  "Start mf6 or switch to it, if it is already running."
  ;; (select-group-or-create ("term" :float nil))
  (gselect "term")
  (run-or-raise (concatenate
		 'string "kitty --class 'mf6' -d" " " path " " "run_mf6")
		'(:title "mf6-stumpwm"))
  (start-mf6-timer path))


(defun is-running-p (props &optional (all-groups *run-or-raise-all-groups*)
                             (all-screens *run-or-raise-all-screens*))
  "Stolen from run-or-raise"
  (let* ((matches (find-matching-windows props all-groups all-screens))
         (other-matches (member (current-window) matches))
         (win (if (> (length other-matches) 1)
                  (second other-matches)
                  (first matches))))
    win))


(defun mf6-slack (path)
  "doc"
  (setf *mf6-running-p* (is-running-p '(:class "mf6")))
  (if (not *mf6-running-p*)
      (let* ((lst (concatenate 'string path "/mfsim.lst"))
	     (tail-cmd (concatenate 'string "tail -n 1 " lst)))
	(if (not (probe-file lst))
	    (progn
	      (echo "fecked like - list file not found")
	      (return-from mf6-slack "done"))
	    (let* ((msg (run-shell-command tail-cmd t))
		   (slack-cmd (concatenate 'string
					   "slack-cli --pre -d slackbot '"
					   msg
					   "'")))
	      (run-shell-command slack-cmd)
	      (echo msg)
	      (cancel-timer *mf6-timer*))))))


(defun mf6-watch (path)
  "Start watching mf6 or switch to it, if it is already running."
  ;; (select-group-or-create ("term" :float nil))
  (gselect "term")

  (run-shell-command (concatenate
		      'string
		      "kitty --class watch -d "
		      path
		      " tail_mf6")))

(defun mf6-mon ()
  "Start htop or switch to it, if it is already running."
  ;; (select-group-or-create ("term" :float nil))
  (gselect "term")
  (run-or-raise "kitty --class htop htop" '(:class "htop")))


(defun mf6-del-win-all ()
  (delete-window (is-running-p '(:class "watch"))))


(defun mf6-all (path)
  "Run the full mf6 thing"
  ;; (select-group-or-create ("term" :float nil))
  (gselect "term")
  (restore-from-file (data-dir-file "term-dump-vertical"))
  (mf6-run path)
  (mf6-watch path)
  (mf6-mon))


(defcommand mf6 () ()
  "Launch an mf6 related process or all"
  (let* ((path (completing-read
		(current-screen)
		"path to mf6 directory: "
		(get-file (data-dir-file "directory-reads"))))
	 (opt (completing-read
	       (current-screen)
	       "option r = run, w = watch, m = monitor, a = all: "
	       '("r" "w" "m" "a")
	       :require-match t)))
   ;; (echo path)
    (cond ((string= opt "r") (mf6-run path))
	  ((string= opt "w") (mf6-watch path))
	  ((string= opt "m") (mf6-mon))
	  ((string= opt "a") (mf6-all path)))))


(define-key *root-map* (kbd "m") "mf6")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; mf6.lisp ends here
