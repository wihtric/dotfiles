;;; init.el --- Emacs config

;; Copyright © 2018 William Witterick <wihtric@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Main init, loads other files

;;; Code:

(add-to-list 'load-path "/home/william/.emacs.d/site-lisp")

(setq custom-file "~/.emacs.d/init/.emacs-custom.el")
(load custom-file 'noerror)

(setq package-enable-at-startup nil)
(package-initialize)
(load-file "~/.emacs.d/init/scratch.el")
(load-file "~/.emacs.d/init/repos.el")
(load-file "~/.emacs.d/init/mail.el")
(load-file "~/.emacs.d/init/org.el")
(load-file "~/.emacs.d/init/im.el")
(load-file "~/.emacs.d/init/dired.el")
(load-file "~/.emacs.d/init/looks.el")
(load-file "~/.emacs.d/init/programming.el")
(load-file "~/.emacs.d/init/modes.el")
(load-file "~/.emacs.d/init/docs.el")
(load-file "~/.emacs.d/init/backups.el")
(load-file "~/.emacs.d/init/search.el")
(load-file "~/.emacs.d/init/yt.el")
(load-file "~/.emacs.d/init/misc.el")
(load-file "~/.emacs.d/init/encrypt.el")
(load-file "~/.emacs.d/init/mf6.el")


;;(load-file "~/.emacs.d/init/conference-image.el")


;;; init.el ends here
