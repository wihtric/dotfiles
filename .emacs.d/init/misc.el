
;;; misc.el --- 
;; 
;; Filename: misc.el
;; Description: 
;; Author: William Witterick
;; Maintainer: 
;; Created: Tue Jan  1 03:58:58 2019 (+0000)
;; Version: 
;; Last-Updated: 
;;           By: 
;;     Update #: 0
;; URL: 
;; Keywords: 
;; Compatibility: 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Commentary: 
;; 
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Change Log:
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Code:



(setq global-auto-revert-mode nil)
(setq auto-revert-use-notify nil)

;; turn off auto revert messages
(setq auto-revert-verbose nil)

(defun insert-rasta-header ()
			    (interactive)
			    (insert-file "~/.rasta_header"))
(global-set-key (kbd "C-c r h")
		'insert-rasta-header)

(add-hook 'csv-mode-hook
	  (lambda()
	    (define-key csv-mode-map (kbd "C-c k") 'csv-kill-fields)
	    (define-key csv-mode-map (kbd "C-c y") 'csv-yank-fields)))



(require 'password-vault)
(password-vault-register-secrets-file "~/data/Dropbox/config/.password.el.gpg")

;;enable column mode WITTW
(setq column-number-mode t)

;;Make it normal!!!!!!
(delete-selection-mode 1)
(setq shift-select-mode t)
(global-set-key [f12] 'toggle-truncate-lines)

;;(require 'no-word)





(defun kill-other-buffers ()
  "Kill all other buffers"
  (interactive)
  (mapc 'kill-buffer (delq (current-buffer) (buffer-list))))

(use-package crux
  :bind (("C-c o" . crux-open-with)
         ("C-a" . crux-move-beginning-of-line)))

(require 'dropbox)


(require 'windmove)
(windmove-default-keybindings 'super)

;; unset C- and M- digit keys
(dotimes (n 10)
  (global-unset-key (kbd (format "C-%d" n)))
  (global-unset-key (kbd (format "M-%d" n)))
  )

(use-package eyebrowse
  :diminish eyebrowse-mode
  :config (progn
            (define-key eyebrowse-mode-map (kbd "M-1") 'eyebrowse-switch-to-window-config-1)
            (define-key eyebrowse-mode-map (kbd "M-2") 'eyebrowse-switch-to-window-config-2)
            (define-key eyebrowse-mode-map (kbd "M-3") 'eyebrowse-switch-to-window-config-3)
            (define-key eyebrowse-mode-map (kbd "M-4") 'eyebrowse-switch-to-window-config-4)
            (eyebrowse-mode t)
            (setq eyebrowse-new-workspace t)))

;; (add-to-list 'load-path "/home/william/.emacs.d/init")
;; (require 'conference-image)

(defun er-byte-compile-init-dir ()
  "Byte-compile all your dotfiles."
  (interactive)
  (byte-recompile-directory user-emacs-directory 0))


(defun clear-undo-tree ()
  (interactive)
  (setq buffer-undo-tree nil))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; misc.el ends here
