;;; looks.el --- Emacs appearance config

;; Copyright © 2018 William Witterick <wihtric@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;
;;; Commentary:
;; Theme, modeline &c

;;; Code:

;; icons
(require 'all-the-icons)

(use-package git-gutter
  :ensure t
  :init
  (global-git-gutter-mode +1))

;; Icon package
(use-package all-the-icons
  :ensure t
  :config
  (use-package all-the-icons-dired
    :ensure t
    :init (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)
    :config (setq all-the-icons-dired-monochrome nil)))

;;(require 'color-theme-sanityinc-tomorrow)
(load-theme 'sanityinc-tomorrow-eighties t)

;; (use-package spacemacs-theme
;;   :ensure t
;;   :no-require t
;;   :init
;;   (load-theme 'spacemacs-dark t)
;;   (setq spacemacs-theme-org-agenda-height nil)
;;   (setq spacemacs-theme-org-height nil))

(use-package spaceline-config
  :ensure spaceline
  )


(use-package spaceline
  :ensure t
  :demand t
  :config
  (require 'spaceline-config)
  (spaceline-toggle-python-env-on))

(use-package all-the-icons
  :ensure t
  )

;;(yahoo-weather-mode)
;;(setq yahoo-weather-guess-location-function #'yahoo-weather-ipinfo)
(setq yahoo-weather-location "Shrewsbury, Shropshire, UK")
;;(yahoo-weather-mode)

(require 'mu4e)

(defun main-weather ()
  (downcase (car (split-string (org-weather-string) ","))))

;;(main-weather)

(spaceline-define-segment ww/all-the-icons-weather
  "An `all-the-icons' segment to display an icon for the current weather"
  (let* ((weather (main-weather))
         (help-echo (format "The weather in `%s' is currently `%s'" yahoo-weather-location weather))
         (icon (all-the-icons-icon-for-weather (downcase weather)))
         (icon-face `(:height ,(spaceline-all-the-icons--height 0.9)
			      :background ,(spaceline-all-the-icons--face-background 'powerline-active2)
			      :inherit)))

    (when (= 1 (length icon)) (setq icon-face (append `(:family ,(all-the-icons-wicon-family)
								:background ,(spaceline-all-the-icons--face-background 'powerline-active2))
						      icon-face)))

    (propertize icon
                'face icon-face
                'display '(raise 0.1)
                'help-echo help-echo
                'mouse-face (spaceline-all-the-icons--highlight)))
  :tight t
)


(spaceline-define-segment ww/all-the-icons-temperature
  "An `all-the-icons' segment to display the current temperature"
  (let* ((yahoo-weather-temperture-format "%d")
         (temperature (nth 1 (split-string (org-weather-string) ",")))
         (icon (if nil "°F" "°C"))
         (icon-face `(:height ,(spaceline-all-the-icons--height 0.9)
                      :family ,(all-the-icons-wicon-family)
                      :foreground ,(ww/spaceline-all-the-icons--temperature-color temperature)
		      ;;:inherit
                      ;;:background ,(spaceline-all-the-icons--face-background 'powerline-active2)
		      ))
         (text-face `(:height ,(spaceline-all-the-icons--height 0.9) :inherit
			      ;;:background ,(spaceline-all-the-icons--face-background 'powerline-active2)
			      )))
    (propertize
     (concat
      (propertize (all-the-icons-wicon "thermometer-exterior") 'face icon-face)
      (unless spaceline-all-the-icons-slim-render (concat
                            (propertize " " 'face `(:height ,(spaceline-all-the-icons--height 0.4) :inherit))
                            (propertize temperature 'face text-face)
                            (propertize icon 'face text-face))))
     ;;'help-echo (format "Temperature is currently %s%s" temperature icon)
     'mouse-face (spaceline-all-the-icons--highlight)
     'display '(raise 0.01)))
  :tight t)


(defun ww/slack-team-get-unread-messages ()

(require 'slack-modeline)
(let
    ((l (slack-team-counts-summary
	 (slack-team-select))))
  (+ (cdr (cdr (car l)))
     (cdr (cdr (car(cdr l)))))))


(spaceline-define-segment ww/all-the-icons-slack
  "An `all-the-icons' segment to display slack unread messages"
  (let* (
	 (unread-num (ww/slack-team-get-unread-messages ))
         (unread-string (number-to-string unread-num))
         (icon-face `(:height ,(spaceline-all-the-icons--height 0.9)
                      :family ,(all-the-icons-faicon-family)
                      :foreground ,(ww/spaceline-all-the-icons--slack-color unread-num)
		      ;;:inherit
                      ;;:background ,(spaceline-all-the-icons--face-background 'powerline-active2)
		      ))
         (text-face `(:height ,(spaceline-all-the-icons--height 0.9) :inherit
			      ;;:background ,(spaceline-all-the-icons--face-background 'powerline-active2)
			      )))
    (propertize
     (concat
      (propertize (all-the-icons-faicon "slack") 'face icon-face)
      (unless spaceline-all-the-icons-slim-render (concat
                            (propertize " " 'face `(:height ,(spaceline-all-the-icons--height 0.4) ;;:inherit 15/02/19
							    ))
                            (propertize unread-string 'face text-face))))
     'help-echo (format "There are %s unread Slack messages " unread-string)
     'mouse-face (spaceline-all-the-icons--highlight)
     'display '(raise 0.1)))
  :tight t)



(defun ww/spaceline-all-the-icons--slack-color (num-messages)
  (if (eq num-messages 0)
      (spaceline-all-the-icons--face-foreground 'powerline-active2)
    "#ff0000"))


(defun ww/spaceline-all-the-icons--temperature-color (temperature)
  "Convert weather INFO into a color temperature Hex Code.
INFO should be an object similar to `yahoo-weather-info'."
  (let* ((yahoo-weather-use-F nil)
         (celsius (string-to-number temperature))
         (normal (max 13 (- 100 (* celsius 4))))
         (clamp (lambda (i) (max 0 (min 255 i))))
         (r (funcall clamp (if (< normal 67)
                               255
                               (* 329.698727446 (expt (- normal 60) -0.1332047592)))))
         (g (funcall clamp (if (< normal 67)
                               (- (* 99.4708025861 (log normal)) 161.1195681661)
                               (* 288.1221695283 (expt (- normal 60) -0.0755148492)))))
         (b (funcall clamp (cond
                            ((> normal 65) 255)
                            ((< normal 20) 0)
                            (t (- (* 138.5177312231 (log (- normal 10))) 305.0447927307))))))
    (format "#%02X%02X%02X" r g b)))

(defun agenda-weather ()
  "Usable as sexp expression in the diary or an org file."
  (all-the-icons-icon-for-weather
   (downcase (car (split-string (org-weather-string date) ","))))
  )

;; this won't work properly for some reason
(setq org-agenda-category-icon-alist
      `(("Org" ,(list (all-the-icons-faicon "cogs")) nil nil :ascent center)
 	("weather" ,(list (all-the-icons-wicon "showers")) nil nil :ascent center)))


(defun ww/mu4e-alert-modeline-formatter (mail-count)
  "Mu4e-alert modeline formatter for spaceline-all-the-icons."
  (when (not (zerop mail-count))
    (let* ((icon (all-the-icons-faicon "envelope" :v-adjust 0.0)))
      (propertize
       (concat
        (propertize icon
                    'face `(:height ,(spaceline-all-the-icons--height 0.9) :inherit)
                    'display '(raise 0.1))
        (propertize (format " %d" mail-count) 'face `(:height ,(spaceline-all-the-icons--height 0.9) :inherit) 'display '(raise 0.1)))
       'help-echo (concat (if (= mail-count 1)
                              "You have an unread email"
                            (format "You have %s unread emails" mail-count))
                          "\nClick here to view "
                          (if (= mail-count 1) "it" "them"))
       'mouse-face (spaceline-all-the-icons--highlight)
       'local-map (make-mode-line-mouse-map 'mouse-1 'mu4e-alert-view-unread-mails)))))



;;(insert (all-the-icons-faicon "slack"))

;; add seg for slack
;; (slack-enable-modeline)
;; (slack-update-modeline)
;; (slack-default-modeline-formatter)
;; (slack-team-get-unread-messages (slack-team-select))
;; slack-modeline-formatter

(use-package spaceline-all-the-icons
  :ensure t
  :after spaceline
  :config
  (spaceline-all-the-icons-theme 'ww/all-the-icons-slack
				 'mu4e-alert-segment
				 ;; 'ww/all-the-icons-weather
				 ;; 'ww/all-the-icons-temperature
				 ) ;;
  (add-hook 'after-init-hook #'fancy-battery-mode)
  (spaceline-all-the-icons--setup-anzu)            ;; Enable anzu searching
  (spaceline-all-the-icons--setup-package-updates) ;; Enable package update indicator
  (spaceline-all-the-icons--setup-git-ahead)       ;; Enable # of commits ahead of upstream in git
  (spaceline-all-the-icons--setup-paradox)         ;; Enable Paradox mode line
  (spaceline-all-the-icons--setup-neotree)         ;; Enable Neotree mode line
  (spaceline-toggle-all-the-icons-vc-status-on)
  (spaceline-toggle-all-the-icons-git-status-on)
;;  (spaceline-toggle-all-the-icons-git-ahead-on)
  (spaceline-toggle-all-the-icons-vc-icon-on)
  (spaceline-toggle-all-the-icons-modified-on)
  (spaceline-toggle-all-the-icons-battery-status-off)
  (spaceline-toggle-all-the-icons-flycheck-status-info-on)
  (spaceline-toggle-all-the-icons-flycheck-status-on)
  (spaceline-toggle-all-the-icons-weather-off)
  (spaceline-toggle-all-the-icons-sunrise-off)
  (spaceline-toggle-all-the-icons-sunset-off)
  (spaceline-toggle-all-the-icons-temperature-off)
  (spaceline-toggle-all-the-icons-buffer-position-off)
  (spaceline-toggle-all-the-icons-hud-off)
  (spaceline-toggle-all-the-icons-time-off)
  (setq spaceline-all-the-icons-separator-type 'none
	spaceline-all-the-icons-hide-long-buffer-path t
	spaceline-all-the-icons-highlight-file-name t
	spaceline-buffer-encoding-abbrev-p t))



(use-package mu4e-alert
  :ensure t
  ;; :after mu4e
  :init
  (mu4e-alert-set-default-style 'libnotify)
  (setq mu4e-alert-interesting-mail-query
	(concat
     "flag:unread maildir:/Inbox"
     ))
  (setq mu4e-alert-modeline-formatter 'ww/mu4e-alert-modeline-formatter)
  (mu4e-alert-enable-notifications)
  (mu4e-alert-enable-mode-line-display)
  ;; (defun refresh-mu4e-alert-mode-line ()
  ;;   (interactive)
  ;;   (mu4e~proc-kill)
  ;;   (mu4e-alert-enable-mode-line-display))
  ;; (run-with-timer 0 60 'refresh-mu4e-alert-mode-line)
  )

;;(mu4e-alert-update-mail-count-modeline)
(run-with-timer 0 (* 1 200) 'mu4e-alert-update-mail-count-modeline)
;;;;; run thi with timwr     mu4e-alert-notify-unread-mail-async)

;;(slack-team-get-unread-messages (slack-team-select))

;; toolbar
(tool-bar-mode -1)
(scroll-bar-mode -1)

(require 'battery)
(display-battery-mode 1)
(menu-bar-mode -99)


;; transparency
 ;;(set-frame-parameter (selected-frame) 'alpha '(<active> . <inactive>))
 ;;(set-frame-parameter (selected-frame) 'alpha <both>)
(set-frame-parameter (selected-frame) 'alpha '(70 . 50))
(add-to-list 'default-frame-alist '(alpha . (70 . 50)))

;; toggle trans
(eval-when-compile (require 'cl))

(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ;; Also handle undocumented (<active> <inactive>) form.
                    ((numberp (cadr alpha)) (cadr alpha)))
              100)
         '(70 . 50) '(100 . 100)))))
(global-set-key (kbd "C-c t") 'toggle-transparency)

 ;; Set transparency of emacs
 (defun transparency (value)
   "Sets the transparency of the frame window. 0=transparent/100=opaque"
   (interactive "nTransparency Value 0 - 100 opaque:")
   (set-frame-parameter (selected-frame) 'alpha value))

;; emojify
(add-hook 'after-init-hook #'global-emojify-mode)

;; excel green
(defface all-the-icons-xlgreen
  '((((background dark)) :foreground "#207245")
    (((background light)) :foreground "#207245"))
  "Face for excel icons"
  :group 'all-the-icons-faces)

(add-to-list 'all-the-icons-icon-alist
	     '("\\.hy$" all-the-icons-fileicon
	      "hy" :height 1.0 :face all-the-icons-dblue))

(add-to-list 'all-the-icons-mode-icon-alist
	     '(hy-mode all-the-icons-fileicon "hy"
		       :height 1.0 :face all-the-icons-dblue))

(add-to-list 'all-the-icons-mode-icon-alist
	     '(yaml-mode all-the-icons-octicon "settings"
		      :height 1.0 :face all-the-icons-dyellow))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.pyx$" all-the-icons-alltheicon
	       "python" :height 1.0 :face all-the-icons-blue))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.pyo$" all-the-icons-alltheicon
	      "python" :height 1.0 :face all-the-icons-yellow))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.pyc$" all-the-icons-alltheicon
	      "python" :height 1.0 :face all-the-icons-red))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.xlsx$" all-the-icons-fileicon
	      "excel" :height 1.0 :face all-the-icons-xlgreen))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.xlsb$" all-the-icons-fileicon
	      "excel" :height 1.0 :face all-the-icons-xlgreen))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.ods$" all-the-icons-fileicon
	      "excel" :height 1.0 :face all-the-icons-xlgreen))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.xls$" all-the-icons-fileicon
	      "excel" :height 1.0 :face all-the-icons-xlgreen))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.xlsm$" all-the-icons-fileicon
	      "excel" :height 1.0 :face all-the-icons-xlgreen))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.qgz$" all-the-icons-fileicon
	       "arch-linux" :height 1.0 :face all-the-icons-lblue))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.shp$" all-the-icons-faicon
	       "globe" :height 1.0 :face all-the-icons-blue))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.qpg$" all-the-icons-faicon
	       "globe" :height 1.0 :face all-the-icons-dblue))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.dbf$" all-the-icons-faicon
	       "globe" :height 1.0 :face all-the-icons-dblue))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.shx$" all-the-icons-faicon
	       "globe" :height 1.0 :face all-the-icons-dblue))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.qix$" all-the-icons-faicon
	       "globe" :height 1.0 :face all-the-icons-dblue))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.qgs$" all-the-icons-fileicon
	       "arch-linux" :height 1.0 :face all-the-icons-lblue))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.rch$" all-the-icons-wicon
	       "rain" :height 0.75 :face all-the-icons-white :v-adjust 0.25))

(add-to-list 'all-the-icons-icon-alist
	     '("\\.nam$" all-the-icons-octicon
	       "settings" :height 1.0 :face all-the-icons-blue))

(add-to-list 'all-the-icons-mode-icon-alist
	     '(modflow-nam-mode all-the-icons-octicon "settings"
		       :height 1.0 :face all-the-icons-blue))

(add-to-list 'all-the-icons-weather-icon-alist
	     '("clear" all-the-icons-wicon "wu-clear"))

(defalias 'list-buffers 'ibuffer)
(setq ibuffer-default-sorting-mode 'major-mode)

;; nearly all of this is the default layout
(setq ibuffer-formats
      '((mark modified read-only " "
              (name 40 40 :left :elide) ; change: 30s were originally 18s
              " "
              (size 9 -1 :right)
              " "
              (mode 16 16 :left :elide)
              " " filename-and-process)
        (mark " "
              (name 16 -1)
              " " filename)))

;;colours
(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
(add-hook 'multi-term-mode-hook 'ansi-color-for-comint-mode-on)

;; time
(setq display-time-day-and-date t)
(setq display-time-24hr-format t)
(display-time-mode 1)

(require 'font-lock+)

(setq inhibit-startup-screen t)
(show-paren-mode 1)
(setq font-lock-maximum-decoration t)

(fset 'yes-or-no-p 'y-or-n-p)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; looks.el ends here
