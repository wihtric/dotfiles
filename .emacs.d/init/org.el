;;; org.el --- 
;; 
;; Filename: org.el
;; Description: 
;; Author: William Witterick
;; Maintainer: 
;; Created: Tue Jan  1 03:59:57 2019 (+0000)
;; Version: 
;; Last-Updated: 
;;           By: 
;;     Update #: 0
;; URL: 
;; Keywords: 
;; Compatibility: 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Commentary: 
;; 
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Change Log:
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Code:



;; (use-package org
;;   :defer t)

;; Ensure ELPA org is prioritized above built-in org.
(require 'cl)
;;(setq load-path (remove-if (lambda (x) (string-match-p "org$" x)) load-path))

;;store org-mode links to messages
(require 'org-mu4e)
;;store link to message if in header view, not to header query
(setq org-mu4e-link-query-in-headers-mode nil)

;; (load-theme 'org-beautify t)
(add-hook 'org-mode-hook (lambda () (load-theme 'org-beautify t)))

(setq org-list-allow-alphabetical t)

(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)


(setq org-support-shift-select t)

(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

(setq org-agenda-files '("~/org/work.org"
			 "~/org/weather.org"
			 "~/org/personal.org"
			 "~/org/birthdays.org"
			 ))

(setq org-agenda-span 14)

(add-to-list 'load-path "~/.emacs.d/site-lisp/org-weather")
;; (require 'org-weather)
;; (setq org-weather-api-key nil)
;; (setq org-weather-format "%main,%tmin, %desc, %tmin-%tmax%tu, %p%pu, %h%hu, %s%su")
;; (setq org-weather-location "Shrewsbury,GB")


;; (run-with-timer 0 (* 60 60) 'org-weather-refresh)

(add-hook 'org-mode-hook
  (lambda()
    (flyspell-mode 1)))

(setq org-export-html-date-format-string "%Y-%m-%d")
(setq org-html-metadata-timestamp-format "%Y-%m-%d")
;;(require 'org-email)

(setq org-src-fontify-natively t)
;; (add-hook 'org-mode-hook 'variable-pitch-mode)

;; (defun my-adjoin-to-list-or-symbol (element list-or-symbol)
;;   (let ((list (if (not (listp list-or-symbol))
;;                   (list list-or-symbol)
;;                 list-or-symbol)))
;;     (require 'cl-lib)
;;     (cl-adjoin element list)))

;; (eval-after-load "org"
;;   '(mapc
;;     (lambda (face)
;;       (set-face-attribute
;;        face nil
;;        :inherit
;;        (my-adjoin-to-list-or-symbol
;;         'fixed-pitch
;;         (face-attribute face :inherit))))
;;     (list 'org-code 'org-block 'org-table 'org-block-background)))

;; this is because of a bug in html export "org-html-fontify-code: Wrong number of arguments"
(defun org-font-lock-ensure ()
  (font-lock-fontify-buffer))

;; active Babel languages
(add-to-list 'org-src-lang-modes '("modflow-nam" . modflow-nam))
(add-to-list 'org-src-lang-modes '("slack-message-buffer" . slack-message-buffer))
(add-to-list 'org-src-lang-modes '("slack-search-result-buffer". slack-search-result-buffer))
;; (require 'hy-font-lock)
(add-to-list 'org-src-lang-modes '("hy" . hy))
(add-to-list 'org-src-lang-modes '("haskell" . haskell))

(org-babel-do-load-languages
 'org-babel-load-languages
 '(
;;   (R . t)
   (emacs-lisp . t)
   (python . t)
   (fortran .t)
   (sql .t)
   (shell .t)
   (hy .t)
   (ditaa . t)
   ))

(setq org-babel-python-command "python")
(setq org-ditaa-jar-path "/usr/share/java/ditaa/ditaa-0.11.jar")


(require 'ox-publish)
;;(add-to-list 'org-latex-packages-alist '("" "listings"))
;;(add-to-list 'org-latex-packages-alist '("" "color"))
(add-to-list 'org-latex-packages-alist '("" "minted"))

(setq org-publish-project-alist
      '(("org-notes"
	 :base-directory "~/org/"
	 :base-extension "org"
	 :publishing-directory "~/public_html/"
	 :recursive t
	 :publishing-function (org-html-publish-to-html org-latex-publish-to-pdf)
	 :headline-levels 5             ; Just the default for this project.
	 :auto-preamble t)

	("org-static"
	 :base-directory "~/org/"
	 :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
	 :publishing-directory "~/public_html/"
	 :recursive t
	 :publishing-function org-publish-attachment)
	("org" :components ("org-notes" "org-static"))))

(require 'table)
(setq org-latex-listings t)
(setq org-latex-listings 'minted)
(setq org-latex-pdf-process
      (quote
       ("pdflatex  -shell-escape -interaction nonstopmode -output-directory %o %f" "pdflatex  -shell-escape -interaction nonstopmode -output-directory %o %f" "pdflatex  -shell-escape -interaction nonstopmode -output-directory %o %f")))

(require 'ox-latex)
(add-to-list 'org-latex-classes
             '("beamer"
               "\\pdfminorversion=4\\documentclass\[presentation\]\{beamer\}"
               ("\\section\{%s\}" . "\\section*\{%s\}")
               ("\\subsection\{%s\}" . "\\subsection*\{%s\}")
               ("\\subsubsection\{%s\}" . "\\subsubsection*\{%s\}")))

;;  (setq beamerpdf  '("beamerpdf" "\\pdfminorversion=4\\documentclass{beamer}" 
;; 		    org-beamer-sectioning))
;; (add-to-list 'org-latex-classes beamerpdf t)


(when window-system
  (custom-set-faces
   '(erc-input-face ((t (:foreground "antique white"))))
   '(helm-selection ((t (:background "ForestGreen" :foreground "black"))))
   '(org-agenda-clocking ((t (:inherit secondary-selection :foreground "black"))) t)
   '(org-agenda-done ((t (:foreground "dim gray" :strike-through nil))))
   '(org-done ((t (:foreground "PaleGreen" :weight normal :strike-through t))))
   '(org-clock-overlay ((t (:background "SkyBlue4" :foreground "black"))))
   '(org-headline-done ((((class color) (min-colors 16) (background dark)) (:foreground "LightSalmon" :strike-through t))))
   '(outline-1 ((t (:inherit font-lock-function-name-face :foreground "cornflower blue" :height 140))))))

;; commented out for now as org-structure-template-alist definition changed in new org mode
;; (require 'ox-reveal)

;; (setq org-reveal-root "file:///home/william/src/reveal.js")
;; (setq org-reveal-title-slide-template "<h1>%t</h1><h2>%a</h2>")

(require 'org-re-reveal)

(setq org-re-reveal-root "file:///home/william/src/reveal.js")


;;(add-to-list 'org-structure-template-alist '("n" . "note"))
(add-to-list 'org-structure-template-alist '("w" . "warning"))

(setq org-directory "~/org")
(setq org-mobile-directory "~/data/Dropbox/MobileOrg")

(setq org-capture-templates
      '(("t" "todo" entry (file+headline "~/org/work.org" "General")
         "* TODO [#A] %?\nSCHEDULED: %(org-insert-time-stamp (org-read-date nil t \"+0d\"))\n%a\n")))

(setq org-agenda-include-diary t)

;; move line up down &c.

(defun move-text-internal (arg)
   (cond
    ((and mark-active transient-mark-mode)
     (if (> (point) (mark))
            (exchange-point-and-mark))
     (let ((column (current-column))
              (text (delete-and-extract-region (point) (mark))))
       (forward-line arg)
       (move-to-column column t)
       (set-mark (point))
       (insert text)
       (exchange-point-and-mark)
       (setq deactivate-mark nil)))
    (t
     (beginning-of-line)
     (when (or (> arg 0) (not (bobp)))
       (forward-line)
       (when (or (< arg 0) (not (eobp)))
            (transpose-lines arg))
       (forward-line -1)))))

(defun move-text-down (arg)
   "Move region (transient-mark-mode active) or current line
  arg lines down."
   (interactive "*p")
   (move-text-internal arg))

(defun move-text-up (arg)
   "Move region (transient-mark-mode active) or current line
  arg lines up."
   (interactive "*p")
   (move-text-internal (- arg)))

(global-set-key [\M-\S-up] 'move-text-up)
(global-set-key [\M-\S-down] 'move-text-down)

;; org export options
(setq org-export-with-sub-superscripts '{})

;; Change the background of source block.
(setq org-html-head "<style>pre.src{background:#F2FBFD;color:#A0C2C2;} </style>")

;; get easy templates back
(use-package org-tempo)

;; (add-to-list 'org-export-options-alist
;; 	     '(:setupfile "SETUPFILE" "theme-readtheorg.setup" t))

(defun setupfile-header ()
  (interactive)
  (insert "#+SETUPFILE: ~/src/org-html-themes/setup/theme-readtheorg.setup"))
(global-set-key (kbd "C-c s h")
		'setupfile-header)

(require 'ox-org)
;;(load "/home/william/src/org-mind-map/org-mind-map.el")


;; to use git cersion not built in
;;(require 'org-install)
(org-reload)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; org.el ends here
