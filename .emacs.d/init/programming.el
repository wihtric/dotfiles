;;; programming.el ---
;;
;; Filename: programming.el
;; Description:
;; Author: William Witterick
;; Maintainer:
;; Created: Tue Jan  1 04:02:46 2019 (+0000)
;; Version:
;; Last-Updated:
;;           By:
;;     Update #: 0
;; URL:
;; Keywords:
;; Compatibility:
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Change Log:
;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

;; get paths etc from bash as we don't get these when starting as daemon
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))
(exec-path-from-shell-copy-env "PATH")
(exec-path-from-shell-copy-env "PYTHONPATH")
(exec-path-from-shell-copy-env "GISBASE")
(exec-path-from-shell-copy-env "GISRC")

(use-package ivy
  :demand t
  :config
  (setq ivy-use-virtual-buffers t
        ivy-count-format "%d/%d "
	ivy-display-style 'fancy))

(ivy-mode)

(projectile-mode +1)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(setq projectile-completion-system 'ivy)
(defadvice projectile-project-root (around ignore-remote first activate)
    (unless (file-remote-p default-directory) ad-do-it))

(use-package counsel-projectile)
(use-package counsel
  :demand t)

;;(add-to-list 'custom-theme-load-path "/home/william/.emacs.d/site-lisp/emacs-color-theme-solarized")
;;(load-theme 'solarized t)



;; multi-term
(global-set-key (kbd "C-c m t") 'multi-term)
(add-hook 'term-mode-hook (lambda ()
			    (define-key term-raw-map
			      (kbd "C-y") 'term-paste)))
(require 'multi-term)
(term-line-mode)

;; temp change this bash broken
(setq multi-term-program "/bin/bash")

(global-set-key (kbd "C-c p") 'shell-pop)




;; lisp
;;(load (expand-file-name "~/quicklisp/slime-helper.el"))

(add-to-list 'load-path "/usr/share/emacs/site-lisp/slime/")
(require 'slime)
(slime-setup '(slime-fancy slime-company))
(setq inferior-lisp-program "sbcl"
      slime-inferior-lisp-args "--load ~/quicklisp/setup.lisp")


;;(slime-connect "127.0.0.1" "4005")

(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook 'highlight-indent-guides-mode)
(add-hook 'yaml-mode-hook 'highlight-indent-guides-mode)

(use-package flycheck
  :ensure t
  :init (setq global-flycheck-mode nil))


;; (when (load "flycheck" t t)
;;   (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
;;   (add-hook 'elpy-mode-hook 'flycheck-mode))

;; (add-hook 'elpy-mode-hook
;;           '(lambda ()
;;              (progn
;;                (setq-local flymake-start-syntax-check-on-newline t)
;;                (setq-local flymake-no-changes-timeout 0.5))))

(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'emacs-lisp-mode-hook       #'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook #'enable-paredit-mode)
(add-hook 'ielm-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-mode-hook             #'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook #'enable-paredit-mode)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)
(add-hook 'hy-mode-hook           #'enable-paredit-mode)

;; ediff
(setq ediff-diff-options "-w")
(setq ediff-split-window-function (quote split-window-horizontally))

;; comments
(global-set-key "\C-c;" 'comment-region)
(global-set-key "\C-x;" 'uncomment-region)

;; assembly
(defun my-asm-mode-hook ()
  ;; you can use `comment-dwim' (M-;) for this kind of behaviour anyway
  (local-unset-key (vector asm-comment-char))
  ;; asm-mode sets it locally to nil, to "stay closer to the old TAB behaviour".
  (setq tab-always-indent (default-value 'tab-always-indent)))

(add-hook 'asm-mode-hook #'my-asm-mode-hook)

;; SQL
(add-hook 'sql-mode-hook 'sql-highlight-postgres-keywords)
;;sql-indent mode
(eval-after-load "sql"
 '(load-library "sql-indent"))

(require 'yasnippet)

(yas-global-mode 0)

(setq yas-snippet-dirs
      '("~/.emacs.d/"                 ;; personal snippets
        "/home/william/src/yasnippet-snippets/snippets/"           ;; foo-mode and bar-mode snippet collection
        "/home/william/src/yasnippet-snippets/snippets" ;; the yasmate collection
        ))


(yas-reload-all)
(add-hook 'prog-mode-hook #'yas-minor-mode)

;;(define-key yas-minor-mode-map (kbd "<tab>") nil)
(define-key yas-minor-mode-map (kbd "<M-tab>") #'yas-expand)
;; (define-key yas-minor-mode-map (kbd "C-c k") nil)

;; (setq yas-snippexot-dirs
;;       '("/home/william/.emacs.d/snippets"                 ;; personal snippets
;;         ))

(require 'eval-sexp-fu)
;;(elpy-enable)




(use-package elpy
  :ensure t
  :defer t
  :init
  (advice-add 'python-mode :before 'elpy-enable)
  :config
(setq python-shell-interpreter "ipython"
      python-shell-interpreter-args "-i --simple-prompt"
      elpy-rpc-python-command "python"
      eldoc-idle-delay 0.5
      ;; elpy-modules (delq 'elpy-module-flymake elpy-modules)
      flycheck-check-syntax-automatically '(save mode-enable mode-enabled)
      flycheck-idle-change-delay 10.0
      eval-sexp-fu-flash-mode nil ;; turn off cos void variable 'hi' error
      )
;;(add-hook 'elpy-mode-hook 'flycheck-mode)

(when (load "flycheck" t t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

)


;; only flycheck on save -- works
;; (eval-after-load "flymake"
;;   '(progn
;;     (defun flymake-after-change-function (start stop len)
;;       "Start syntax check for current buffer if it isn't already running."
;;       ;; Do nothing, don't want to run checks until I save.
;;       )))

;; (setq flycheck-check-syntax-automatically '(save mode-enable idle-change mode-enabled))
;; (setq flycheck-idle-change-delay 10.0)


;; the default value was '(save idle-change new-line mode-enabled)

;;(setq elpy-rpc-python-command "python")

(setq python-check-command "flake8-python")
;; (elpy-use-ipython)

;;(setq elpy-rpc-backend "jedi")


;; (setq python-shell-interpreter "jupyter"
;;       python-shell-interpreter-args "console --simple-prompt"
;;       python-shell-prompt-detect-failure-warning nil)
;; (add-to-list 'python-shell-completion-native-disabled-interpreters
;;              "jupyter")

(require 'auto-virtualenvwrapper)
(add-hook 'python-mode-hook #'auto-virtualenvwrapper-activate)
(add-hook 'projectile-after-switch-project-hook #'auto-virtualenvwrapper-activate)


;;(add-to-list 'auto-mode-alist '("\\.pyx\\'" . python-mode))
(require 'flycheck-cython)
(add-hook 'cython-mode-hook 'flycheck-mode)


(setq geiser-active-implementations '(guile))

;; make
(setq compile-command "make"
      compilation-scroll-output 1)
(global-set-key (kbd "C-c c") 'compile)


(require 'linum)
(add-hook 'fortran-mode-hook
	  (lambda() (linum-mode 1)
	    (auto-fill-mode 0)                   ; turn off auto-filling
	    (setq fortran-do-indent 2) (setq fortran-if-indent 2)
	    (setq fortran-type-indent 2) (setq fortran-program-indent 2)
	    (setq fortran-associate-indent 2) (setq fortran-critical-indent 2)
	    (setq fortran-continuation-indent 4)))

(autoload 'f90-mode "f90" "Fortran 90 mode" t)
(add-hook 'f90-mode-hook 'my-f90-mode-hook)

(defun my-f90-mode-hook ()
  (setq f90-font-lock-keywords f90-font-lock-keywords-3)
  (abbrev-mode 1)                       ; turn on abbreviation mode
  (turn-on-font-lock)                   ; syntax highlighting
  (linum-mode 1)                        ; shaolin line numbers
  (auto-fill-mode 0)                   ; turn off auto-filling
  (setq f90-do-indent 2) (setq f90-if-indent 2)
  (setq f90-type-indent 2) (setq f90-program-indent 2)
  (setq f90-associate-indent 2) (setq f90-critical-indent 2)
  (setq f90-continuation-indent 4) (yas-minor-mode))

(prettify-symbols-mode 0)
(require 'pretty-lambdada)

(add-hook 'emacs-lisp-mode-hook (lambda()
				  ( pretty-lambda-mode 1)
				  ;;(yas-minor-mode)
				  (flycheck-mode)
				  (company-mode)
				  (company-quickhelp-mode 1)
				  (auto-make-header)))

(defun company-yasnippet-or-completion ()
  "Solve company yasnippet conflicts."
  (interactive)
  (let ((yas-fallback-behavior
         (apply 'company-complete-common nil)))
    (yas-expand)))

(add-hook 'company-mode-hook
          (lambda ()
            (substitute-key-definition
             'company-complete-common
             'company-yasnippet-or-completion
             company-active-map)))

(add-hook 'python-mode-hook (lambda()
			      ;; (pretty-lambda-mode 1)
			      ;; (company-mode)
			      ;; (yas-minor-mode)
			      ;; (flycheck-mode)
			      ;; (company-quickhelp-mode 1)
			      ))

;; (add-hook 'python-mode-hook (lambda()
;; 			      (pretty-lambda-mode 1)))


;; (add-hook 'hy-mode-hook (lambda()
;; 			      ;;(yas-minor-mode)
;; 			      (company-mode)
;; 			      (company-quickhelp-mode 1)))

(setq hy-shell--interpreter-args nil)

(add-hook 'scheme-mode-hook (lambda()
			      (company-mode)
			      (pretty-lambda-mode 1)
			      ;;(yas-minor-mode)
			      ))

(add-hook 'lisp-mode-hook (lambda()
			    ( pretty-lambda-mode 1)
			    ;;(yas-minor-mode)
			    (company-mode)
			    (company-quickhelp-mode 1)
			    (auto-make-header)))


(add-hook 'after-init-hook 'global-company-mode)

(add-hook 'geiser-mode-hook
 	  (lambda()
	    (pretty-lambda-mode 1)))

(add-hook 'geiser-repl-mode-hook
 	  (lambda()
	    (pretty-lambda-mode 1)))

;; rotate
(defvar rotate-text-rotations
  '(("True" "False")
    ("yes" "no")
    ("disabled" "enabled")
    ("true" "false"))
  "List of text rotation sets.")

;; (setq rotate-text-rotations
;;       '(("True" "False")
;; 	("yes" "no")
;; 	("disabled" "enabled")
;; 	("true" "false")))

(defun rotate-region (beg end)
  "Rotate all matches in `rotate-text-rotations' between point and mark."
  (interactive "r")
  (let ((regexp (rotate-convert-rotations-to-regexp
		 rotate-text-rotations))
	(end-mark (copy-marker end)))
    (save-excursion
      (goto-char beg)
      (while (re-search-forward regexp (marker-position end-mark) t)
	(let* ((found (match-string 0))
	       (replace (rotate-next found)))
	  (replace-match replace))))))

(defun rotate-string (string &optional rotations)
  "Rotate all matches in STRING using associations in ROTATIONS.
If ROTATIONS are not given it defaults to `rotate-text-rotations'."
  (let ((regexp (rotate-convert-rotations-to-regexp
		 (or rotations rotate-text-rotations)))
	(start 0))
    (while (string-match regexp string start)
      (let* ((found (match-string 0 string))
	     (replace (rotate-next
		       found
		       (or rotations rotate-text-rotations))))
	(setq start (+ (match-end 0)
		       (- (length replace) (length found))))
	(setq string (replace-match replace nil t string))))
    string))

(defun rotate-next (string &optional rotations)
  "Return the next element after STRING in ROTATIONS."
  (let ((rots (rotate-get-rotations-for
	       string
	       (or rotations rotate-text-rotations))))
    (if (> (length rots) 1)
	(error (format "Ambiguous rotation for %s" string))
      (if (< (length rots) 1)
	  ;; If we get this far, this should not occur:
	  (error (format "Unknown rotation for %s" string))
	(let ((occurs-in-rots (member string (car rots))))
	  (if (null occurs-in-rots)
	      ;; If we get this far, this should *never* occur:
	      (error (format "Unknown rotation for %s" string))
	  (if (null (cdr occurs-in-rots))
	      (caar rots)
	    (cadr occurs-in-rots))))))))

(defun rotate-get-rotations-for (string &optional rotations)
  "Return the string rotations for STRING in ROTATIONS."
  (remq nil (mapcar (lambda (rot) (if (member string rot) rot))
		    (or rotations rotate-text-rotations))))

(defun rotate-convert-rotations-to-regexp (rotations)
  (regexp-opt (rotate-flatten-list rotations)))

(defun rotate-flatten-list (list-of-lists)
  "Flatten LIST-OF-LISTS to a single list.
Example:
  (rotate-flatten-list '((a b c) (1 ((2 3)))))
    => (a b c 1 2 3)"
  (if (null list-of-lists)
      list-of-lists
    (if (listp list-of-lists)
	(append (rotate-flatten-list (car list-of-lists))
		(rotate-flatten-list (cdr list-of-lists)))
      (list list-of-lists))))

(defun rotate-word-at-point ()
  "Rotate word at point based on sets in `rotate-text-rotations'."
  (interactive)
  (let ((bounds (bounds-of-thing-at-point 'word))
        (opoint (point)))
    (when (consp bounds)
      (let ((beg (car bounds))
            (end (copy-marker (cdr bounds))))
        (rotate-region beg end)
        (goto-char (if (> opoint end) end opoint))))))

(global-set-key (kbd "C-c '")  'rotate-word-at-point)
(put 'upcase-region 'disabled nil)


(autoload 'markdown-mode "markdown-mode"
   "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist '("\\.text\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))

(require 'hy-mode)

(require 'common-lisp-snippets)
(common-lisp-snippets-initialize)

(use-package vterm
  :ensure t
  :init (setq vterm-shell "fish")
)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; programming.el ends here
