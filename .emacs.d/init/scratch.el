;;; scratch.el ---
;; 
;; Filename: scratch.el
;; Description: 
;; Author: William Witterick
;; Maintainer: 
;; Created: Tue Jan  1 04:04:35 2019 (+0000)
;; Version: 
;; Last-Updated: 
;;           By: 
;;     Update #: 0
;; URL: 
;; Keywords: 
;; Compatibility: 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Commentary: 
;; 
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Change Log:
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Code:




;;(setq initial-major-mode 'fundamental-mode)
(setq initial-scratch-message "\
;; Shift-AltGr for compose
;; C-x M-o dired hide hidden
;; 
;; M-l
;; Convert following word to lower case (downcase-word). 
;; M-u
;; Convert following word to upper case (upcase-word). 
;; M-c
;; Capitalize the following word (capitalize-word). 
;; C-x C-l
;; Convert region to lower case (downcase-region). 
;; C-x C-u
;; Convert region to upper case (upcase-region).
;; mu4e C1-c add CC

;; export vcal to diary 
;; C-v C-e 

;; quit org agenda
;; x

;; tangle single block
;; C-u C-c C-v t

;; PV vectors
;; (dirx*iHat) + (diry*jHat) + (dirz*kHat);

;; ivy-immediate-done
;; C-M-j

;; projectile

;;    Find file in current project (s-p f)
;;    Switch project (s-p p)
;;    Grep in project (s-p s g)
;;    Replace in project (s-p r)
;;    Invoke a command via the Projectile Commander (s-p m)

;;; emacsclient -e '(mu4e-update-index)'


;;; search slack

;    search-term after:2019-09-01 before:2019-09-04
;    search-term after:2019-09-01 before:2019-09-02 from:@wihtric from:@Al
;    search-term on:2019-09-02
;    a on:2019-09-02 (for all messages)

;;;; find file by date

;; find /home/william/scripts -type f -name something_here -newermt 2019-09-01 ! -newermt 2019-09-03


;;;; monitor

;  tail -f /var/log/pacman.log | bat --paging=never


;;;; slime

;  C-c M-o slime-repl-clear-buffer


;; I'm using mu4e for email. C-c c-a calls mml-attach-file.
;; Is there a way of getting rid of the three questions it asks

;; I use mail-add-attachment, it doesn't ask any questions.

;;  Oh, brilliant, you're right. I'll just rebind it. Cheers.

;; webex fivewebex!

"					      )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; scratch.el ends here
