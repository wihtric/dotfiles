;;; modes.el --- 
;; 
;; Filename: modes.el
;; Description: 
;; Author: William Witterick
;; Maintainer: 
;; Created: Tue Jan  1 03:56:38 2019 (+0000)
;; Version: 
;; Last-Updated: 
;;           By: 
;;     Update #: 0
;; URL: 
;; Keywords: 
;; Compatibility: 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Commentary: 
;; 
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Change Log:
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Code:


(setq mf-keywords
      '("BAS6"
	"BAS"
	"BCF"
	"DIS"
	"DISU"
	"DISV"
	"BCF6"
	"STR"
	"RCH"
	"EVT"
	"BINARY"
	"WEL"
	"CLN"
	"LPF"
	"GHB"
	"OC"
	"LIST"
	"MNW1"
	"MNW2"
	"PCG"
	"SMS"
	"BCT"
	"PCB"
	"REPLACE"
	"UPW"
	"NWT"
	"LMT6"
	"DRN"
	"GNC"
	"HFB"
	"RIV"
	"CHD"
	"SFR"
	"TVM"
	"TDIS6"
	"IMS6"
	"GWF6"
	"DISU6"
	"DIS6"
	"DRN6"
	"IC6"
	"OC6"
	"NPF6"
	"STO6"
	"EVT6"
	"SFR6"
	"RCH6"
	"WEL6"
	"SFR6"
	"OBS6"
	"TS6"
	"CHD6"
	"HFB6"
	"INTERNAL"
	"CONSTANT"
	"SAVE"
	"PRINT"
	"VKD6"
	"SIMPLE"
	"STEPWISE"
	"UNDER_RELAXATION"
	"RIV6"
	"GHB6"
	"BUD"
	"ZON"
	"GRB"
	"ALL"
	"SUMMARY"
	"COMPLEX"
	"SIMPLE"))


(require 'generic-x)

(define-generic-mode
  'conflow-dat                          ;; name of the mode
  '("/*" "*/")                           ;; comments delimiter
   nil   ;; some keywords

  '(("=" . 'font-lock-operator)
    ("+" . 'font-lock-operator)     ;; some operators
    (";" . 'font-lock-builtin)     ;; a built-in
    ("[0-9]+" . 'font-lock-variable-name-face)
    ("\\('.*'\\)" . 'font-lock-doc-face)
    ("\\(>>.*\\)" . 'font-lock-function-name-face))
  '("\\.dat$")                    ;; files that trigger this mode
   nil                              ;; any other functions to call
  "Conflow dataset minimal highlighting"     ;; doc string
)


(define-generic-mode
  'modflow-nam-mode                          ;; name of the mode
  '("#" "#")                           ;; comments delimiter
  mf-keywords
  '(("=" . 'font-lock-operator)
    ("+" . 'font-lock-operator)     ;; some operators
    (";" . 'font-lock-builtin)     ;; a built-in
;;    ("METRES" . 'font-lock-builtin)     ;; a built-in
    ;; (" [0-9]+.*" . 'font-lock-builtin)     ;; a built-in
    ("MAXBOUND\\|NSEG\\|ICELLTYPE\\|ICONVERT\\|TOP\\|SS\\|SY\\| K \\| K33 \\|HEAD\\|STRT\\|BOT.*\\|IAC\\|AREA\\|JA\\|IHC\\|CL12\\|HWVA\\|NLAY\\|DELR\\|DELC\\|BOTM\\|IDOMAIN\\|NROW\\|NCOL\\|NEWTON\\|SAVE_FLOWS\\|UNIT_CONVERSION\\|NREACHES\\|STAGE\\|STATUS\\|RUNOFF\\|INFLOW\\|FILEIN\\|FILEOUT\\|AUTO_FLOW_REDUCE\\|NAMES\\|METHOD\\|METHODS\\|MAXHFB\\|NCELLS\\|IZONE\\| BUDGET \\|NUMELEVS\\|NUMVKD\\|PRINT_OPTION\\|COMPLEXITY\\|OUTER_MAXIMUM\\|UNDER_RELAXATION_GAMMA\\|UNDER_RELAXATION_KAPPA\\|UNDER_RELAXATION_MOMENTUM\\|BACKTRACKING_REDUCTION_FACTOR\\|INNER_MAXIMUM" . 'font-lock-variable-name-face)
    ("\\('.*'\\)" . 'font-lock-doc-face)
    ("\\|METRES\\|DAYS" . 'font-lock-constant-face)
    ("BEGIN.*\\|END.*." . 'font-lock-function-name-face)
    ;("BEGIN.END" . 'font-lock-function-name-face)
;
)
  '("\\.nam$"
    "\\.tdis$"
    "\\.ims$"
    "\\.disu$"
    "\\.ic$"
    "\\.ts$"
    "\\.oc$"
    "\\.npf$"
    "\\.sto$"
    "\\.chd$"
    "\\.ghb$"
    "\\.riv$"
    "\\.evt$"
    "\\.sfr$"
    "\\.rch$"
    "\\.hfb$"
    "\\.zon$"
    "\\.wel$"
    "\\.vkd$"
    "\\.obs$"
    "\\.dis$")                    ;; files that trigger this mode
   nil                              ;; any other functions to call
  "Modflow name file minimal highlighting"     ;; doc string
)


(define-generic-mode
  'modflow-list                          ;; name of the mode
  '("#")                           ;; comments delimiter
   mf-keywords   ;; some keywords

  '(("=" . 'font-lock-operator)
    ("+" . 'font-lock-operator)     ;; some operators
    ("-" . 'font-lock-builtin)     ;; a built-in
    ("[0-9]+" . 'font-lock-variable-name-face)
    ("\\('.*'\\)" . 'font-lock-doc-face)
    ("\\(-.*\\)" . 'font-lock-function-name-face))
  '("\\.lst$"
    "\\.list")                    ;; files that trigger this mode
   nil                              ;; any other functions to call
  "modflow outout minimal highlighting"     ;; doc string
)


(setq mymath-highlights
      '(("Sin\\|Cos\\|Sum" . font-lock-function-name-face)
        ("Pi\\|Infinity" . font-lock-constant-face)))


(use-package undo-tree
  :demand t
  :config
  (global-undo-tree-mode))

(use-package ws-butler
  :demand t
  :commands (ws-butler-global-mode)
  :hook (after-init . (lambda () (ws-butler-global-mode 1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; modes.el ends here
