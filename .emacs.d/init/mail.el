;;; mail.el --- Emacs mail config

;; Copyright © 2018 William Witterick <wihtric@gmail.com>

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Work email mu4e

;;; Code:

(require 'smtpmail)


(use-package mu4e

  :config
  (setq mu4e-attachment-dir  "/scratch")
  (setq mu4e-headers-skip-duplicates t)
  (setq mu4e-update-interval 30)
  (setq mu4e-hide-index-messages 1)
;;  (setq mu4e-get-mail-command "mbsync work")
  (setq mu4e-get-mail-command "true")
  (setq mu4e-change-filenames-when-moving t)
  (setq mu4e-maildir (expand-file-name "~/.mail/work"))


  (setq
   mu4e-sent-folder   "/Sent"       ;; folder for sent messages
   mu4e-drafts-folder "/Drafts"     ;; unfinished messages
   mu4e-refile-folder "/Archives"
   mu4e-trash-folder  "/Trash")      ;; trashed messages
  (setq mu4e-headers-visible-lines 30)


  )



;; (require 'mu4e)
;; (setq mu4e-attachment-dir  "/scratch")
;; (setq mu4e-headers-skip-duplicates t)
;; ;(setq mu4e-update-interval (* 1 60))
;; (setq mu4e-update-interval nil)

;; (setq mu4e-hide-index-messages 1)



(require 'org-mu4e)

;;command used to get mail
;; use this for testing
;;(setq mu4e-get-mail-command "true")
;; use this to sync with msbynnc
;;(setq mu4e-get-mail-command "mbsync work")
;;(setq mu4e-get-mail-command "true")

;;rename files when moving
;;NEEDED FOR MBSYNC
;;(setq mu4e-change-filenames-when-moving t)

;; (setq mu4e-index-cleanup t
;;       mu4e-index-lazy-check nil)

;;set up queue for offline email
;;use mu mkdir  ~/Maildir/queue to set up first
(setq smtpmail-queue-mail nil  ;; start in normal mode
      smtpmail-queue-dir   "~/.mail/work/queue/cur")

(setq message-send-mail-function 'smtpmail-send-it
      starttls-use-gnutls t
      smtpmail-starttls-credentials
      '(("smtp.office365.com" 587 nil nil))
      smtpmail-auth-credentials
      (expand-file-name "~/.authinfo.gpg")
      smtpmail-default-smtp-server "smtp.office365.com"
      smtpmail-smtp-server "smtp.office365.com"
      smtpmail-smtp-service 587
      smtpmail-debug-info t
      user-mail-address "w.witterick@gwscience.co.uk")

;;location of my maildir
;; (setq mu4e-maildir (expand-file-name "~/.mail/work"))


;; (setq
;;   mu4e-sent-folder   "/Sent"       ;; folder for sent messages
;;   mu4e-drafts-folder "/Drafts"     ;; unfinished messages
;;   mu4e-refile-folder "/Archives"
;;   mu4e-trash-folder  "/Trash")      ;; trashed messages

;; (setq mu4e-headers-visible-lines 30)

(autoload 'mu-cite-original "mu-cite" nil t)
;; for all but message-mode
;; (add-hook 'mail-citation-hook (function mu-cite-original))
;; for message-mode only
(setq message-cite-function (function mu-cite-original))

;; bbdb
(setq bbdb-file "~/data/Dropbox/contacts/.bbdb")

;;(require 'bbdb-loaddefs "/home/william/.emacs.d/site-lisp/bbdb/lisp/bbdb-loaddefs.el")
;;(require 'bbdb)
(bbdb-initialize)
(setq bbdb-always-add-address t
      bbdb-offer-save 1   ;; save without asking
      bbbd-message-caching-enabled t           ;; be fast
      bbdb-use-alternate-names t               ;; use AKA
      bbdb-completion-display-record nil)

(require 'bbdb-csv-import)
(require 'bbdb-vcard)
(setq bbdb-csv-import-mapping-table bbdb-csv-import-thunderbird)


(add-hook 'gnus-startup-hook 'bbdb-insinuate-gnus)

(setq user-mail-address "w.witterick@gwscience.co.uk"
      user-full-name "William Witterick")

(defun mbork/message-attachment-present-p ()
  "Return t if an attachment is found in the current message."
  (save-excursion
    (save-restriction
      (widen)
      (goto-char (point-min))
      (when (search-forward "<#part" nil t) t))))

(defcustom mbork/message-attachment-intent-re
  (regexp-opt '("I attach"
		"I have attached"
		"I've attached"
		"I have included"
		"I've included"
		"see the attached"
		"see the attachment"
		"attached file"
		"I have appended"))
  "A regex which - if found in the message, and if there is no
attachment - should launch the no-attachment warning.")

(defcustom mbork/message-attachment-reminder
  "Are you sure you want to send this message without any attachment? "
  "The default question asked when trying to send a message
containing `mbork/message-attachment-intent-re' without an
actual attachment.")

(defun mbork/message-warn-if-no-attachments ()
  "Ask the user if s?he wants to send the message even though
there are no attachments."
  (when (and (save-excursion
	       (save-restriction
		 (widen)
		 (goto-char (point-min))
		 (re-search-forward mbork/message-attachment-intent-re nil t)))
	     (not (mbork/message-attachment-present-p)))
    (unless (y-or-n-p mbork/message-attachment-reminder)
      (keyboard-quit))))

(add-hook 'message-send-hook #'mbork/message-warn-if-no-attachments)

;; diary
(defun import-vcal-attachment ()
  (interactive)
  (icalendar-import-file "/home/william/.attachments/vcal-2.vcs"
			 "/home/william/.emacs.d/diary"))

(define-key mu4e-view-mode-map (kbd "C-v C-e") 'import-vcal-attachment)

(setq mu4e-view-show-addresses t
        mu4e-compose-signature (concat
                                 "William Witterick\n"
                                 "Groundwater Science Ltd.\n"
                                 "Tel: +44 (0)7837299703\n"))


(require 'gnus-dired)
;; make the `gnus-dired-mail-buffers' function also work on
;; message-mode derived modes, such as mu4e-compose-mode
(defun gnus-dired-mail-buffers ()
  "Return a list of active message buffers."
  (let (buffers)
    (save-current-buffer
      (dolist (buffer (buffer-list t))
        (set-buffer buffer)
        (when (and (derived-mode-p 'message-mode)
                (null message-sent-message-via))
          (push (buffer-name buffer) buffers))))
    (nreverse buffers)))

(setq gnus-dired-mail-mode 'mu4e-user-agent)
(add-hook 'dired-mode-hook 'turn-on-gnus-dired-mode)

;; C-1 c for CC
(define-key mu4e-compose-mode-map (kbd "C-1 c") 'message-goto-cc)

;; bbdb mu4e
(setq bbdb-mail-user-agent (quote message-user-agent))
(setq mu4e-view-mode-hook (quote (bbdb-mua-auto-update visual-line-mode)))
(setq mu4e-compose-complete-addresses nil)
(setq bbdb-mua-pop-up nil)
(setq bbdb-mua-pop-up-window-size 5)
(setq mu4e-save-multiple-attachments-without-asking t)
(add-hook 'mu4e-compose-mode-hook
  (lambda()
    (flyspell-mode 1)))

;; ;;;;; HTML email

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;; END HTML

(imagemagick-register-types)

;; enable inline images
(setq mu4e-view-show-images t)
;; use imagemagick, if available
(imagemagick-register-types)

(add-hook 'message-setup-hook 'bbdb-mail-aliases)
(setq bbdb-completion-display-record nil)
;; (sr-speedbar-open)

(setq bbdb-mua-auto-update-p 'nil)

(setq mu4e-attachment-dir  "~/.attachments")


;;;;;;;; calendar

;; configure excorporate
;; allow opening the exchange calendar with 'e' from calendar
;;(evil-define-key 'motion calendar-mode-map "e" #'exco-calendar-show-day)




(setq-default
    ;; configure email address and office 365 exchange server adddress for exchange web services
 excorporate-configuration
 (quote
  ("w.witterick@gwscience.co.uk" . "https://outlook.office365.com/EWS/Exchange.asmx"))
 ;; integrate emacs diary entries into org agenda
 org-agenda-include-diary t
 )



;; activate excorporate and request user/password to start connection
;;(excorporate)

;; enable the diary integration
;;(i.e. write exchange calendar to emacs diary file -> ~/.emacs.d/diary must exist)
;;(excorporate-diary-enable)
(defun ab/agenda-update-diary ()
  "call excorporate to update the diary for today"
  (interactive)
  (exco-diary-diary-advice (calendar-current-date) (calendar-current-date) #'message "diary updated"))






   ;; update the diary every time the org agenda is refreshed
   ;;(add-hook 'org-agenda-cleanup-fancy-diary-hook 'ab/agenda-update-diary )




;;; mail.el ends here
