;;; im.el ---
;; 
;; Filename: im.el
;; Description: 
;; Author: William Witterick
;; Maintainer: 
;; Created: Tue Jan  1 03:57:35 2019 (+0000)
;; Version: 
;; Last-Updated: 
;;           By: 
;;     Update #: 0
;; URL: 
;; Keywords: 
;; Compatibility: 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Commentary: 
;; 
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;; Change Log:
;; 
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;; 
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;; 
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; -*- lexical-binding: t -*-
;; -*- epa-file-encrypt-to: ("wihtric@gmail.com") -*-
;;
;;; Code:

(use-package slack

  :commands (slack-start)
  :bind (:map slack-mode-map
              (("@" . slack-message-embed-mention)
               ("#" . slack-message-embed-channel)))

  :init
  (setq slack-buffer-emojify t)
  (setq slack-prefer-current-team t)
  (setq slack-render-image-p t)
  (setq slack-buffer-create-on-notify t)

  :config
  (slack-register-team
   :name "Groundwater Science"
   :default t
   :client-id xxx
   :client-secret xxx
   :token xxx
   :disable-block-format t
   :modeline-enabled t
   ;; :subscribed-channels '(("gs121_nkgw" "office" "z_hurlyburly" "gs116_vergade" "gs186_eshrop" "gs214_yapton"))
   )

  ;; set presence to auto
  (slack-request-set-presence (slack-team-select) "auto")

  ;; set status to working
  (defvar slack-work-string "in the workshop")
  (defvar slack-status-alist '(("re-fuelling" . ":hot_pepper:")
			       ("tea" . "🍵")
			       ("phone" . ":telephone:")
			       ("away" . "☀")
			       ("shops" . "🛒")
			       ("toilet" . ":toilet:")
			       ("beer". 🍺)))

  (add-to-list 'slack-status-alist `(,slack-work-string . ":hammer_and_wrench:"))

  (defun slack-set-status (status)
    "set your fecking status"
    (let* ((result (assoc status slack-status-alist)))
      (slack-user-set-status-request
       (slack-team-select)
       (cdr result)
       (car result))
      (alert (concat "Slack status - " (car result))))))

(use-package alert
  :commands (alert)
  :init
  (setq alert-default-style 'notifications)
  (setq alert-fade-time 30))

(defun slack-message-notify-alert (message room team)
  "Hello msg to alert if email (MESSAGE ROOM TEAM)."
  (if (slack-message-notify-p message room team)
      (let ((team-name (oref team name))
            (room-name (slack-room-name room team))
            (text (with-temp-buffer
                    (goto-char (point-min))
                    (insert (slack-message-to-alert message team))
                    (slack-buffer-buttonize-link)
                    (buffer-substring-no-properties (point-min)
                                                    (point-max))))
            (user-name (slack-message-sender-name message team)))
        (if (and (eq alert-default-style 'notifier)
                 (slack-im-p room)
                 (or (eq (aref text 0) ?\[)
                     (eq (aref text 0) ?\{)
                     (eq (aref text 0) ?\<)
                     (eq (aref text 0) ?\()))
            (setq text (concat "\\" text)))
        (alert (if (slack-im-p room) text (format "%s: %s" user-name text ))
               :icon slack-alert-icon
               :title (if (slack-im-p room)
                          (funcall slack-message-im-notification-title-format-function
                                   team-name room-name (slack-thread-messagep message))
                        (funcall slack-message-notification-title-format-function
                                 team-name room-name (slack-thread-messagep message)))
               :category 'slack))))

(defun ww/slack-set-status-select ()
  "Set slack status."
  (interactive)
  (let* ((status (completing-read "Select status "
                                  slack-status-alist
                                  nil
                                  t)))
	 (slack-set-status status)))

(defun ww/slack-quote-region (text)
    (with-temp-buffer
      (insert text)
      (goto-char 1)
      (while (> (point-max) (point))
        (beginning-of-line)
        (insert "> ")
        (forward-line 1))
      (buffer-string)))

(defun ww/decorate-text (text)
  (let* ((decorators '(("None" . (lambda (text) text))
                       ("Code"  . (lambda (text) (concat "```" text "```")))
		       ("Quote" . (lambda (text) (ww/slack-quote-region text)))))
         (decoration (completing-read "Select decoration: "
                                      decorators
                                      nil
                                      t)))
    (funcall (cdr (assoc decoration decorators)) text)))

(defun ww/slack-send-region-to-slack ()
  (interactive)
  (let* ((team (slack-team-select))
         (room (slack-room-select
                (cl-loop for team in (list team)
                         append (with-slots (groups ims channels) team
                                  (append ims groups channels)))
		team)))
    (slack-message-send-internal (ww/decorate-text (filter-buffer-substring
                                                    (region-beginning) (region-end)))
                                 room team)))

(slack-start)

(setq slack-modeline-count-only-subscribed-channel nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; im.el ends here
